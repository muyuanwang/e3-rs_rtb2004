#include <string.h>    // Provides memcpy prototype
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#include <registryFunction.h>

#define MAXPOINTS 200001
/*
 *  parsing waveform
 *
 * Parameters:
 *
 *  INA: measurement control
 *  INB: channel status
 *  INC: xstart
 *  IND: xstop
 *  INE: raw data points
 *  INF: yincrement
 *  ING: yorigin
 *  INH: format for fetching raw data
 *  INI: raw5 (5 digits to skip over for head string)
 *  INJ: raw6 (6 digits to skip over for head string)
 *
 *  VALA: xaxis
 *  VALB: yaxis
 *
 */
static int parsing_waveform_uint16(aSubRecord *precord)
{
    const short mesWF	= *((short*)precord->a);
    const short channel	= *((short*)precord->b);
    const long points	= *((long*)precord->e);
    if (points>MAXPOINTS) {
        printf("[aSub ERROR] data points is too high\n");
        return -1;
    }
    if (points>precord->nova) {
        printf("[aSub Error] data points is bigger than NOVA");
        return -1;
    }
    // parse parameters
    const double xstart	= *((double*)precord->c);
    const double xstop  = *((double*)precord->d);
    const double yinc	= *((double*)precord->f);
    const double yor	= *((double*)precord->g);
    const short format	= *((short*)precord->h);
    double *xaxis, *yaxis;
    double *input_wave;
    // parsing waveform
    if (mesWF==0 || (mesWF==1 && channel==0)) {
        memset(precord->vala, 0, precord->nova*sizeof(double));
        precord->neva=0;
        memset(precord->valb, 0, precord->novb*sizeof(double));
        precord->nevb=0;
    }
    else if (mesWF==1 && channel==1) {
        int i;
        xaxis = (double *)malloc(points*sizeof(double));
        if (xaxis == NULL) {
            printf("[asub Error] cannot allocate memory");
            return -1;
        }
        yaxis = (double *)malloc(points*sizeof(double));
	if (yaxis == NULL) {
            printf("[asub Error] cannot allocate memory");
            return -1;
        }
        if (format==0) {
            input_wave = (double*)precord->i;
        }
//            for (i=0; i<points; i++) {
//                xaxis[i]=xstart+i*(xstop-xstart)/points;
//                yaxis[i]=input_wave[i]*yinc+yor;
//            }
        
        else if (format==1) {
            input_wave = (double*)precord->j;
        }
//            for (i=0; i<points; i++) {
//                xaxis[i]=xstart+i*(xstop-xstart)/points;
//                yaxis[i]=input_wave[i]*yinc+yor;
//            }
        
        for (i=0; i<points; i++) {
            xaxis[i]=xstart+i*(xstop-xstart)/points;
            yaxis[i]=input_wave[i]*yinc+yor;
        }

//        else {
//            printf("[aSub ERROR] Invalid raw data\n");
//            return -1;
//        }
        memcpy(precord->vala,xaxis,points*sizeof(double));
        precord->neva=points;
        memcpy(precord->valb,yaxis,points*sizeof(double));
        precord->nevb=points;
        free(xaxis);
        free(yaxis);
    }
    /* Done! */
    return 0;
}

/*
 *  copying waveform
 *
 * Parameters:
 *
 *  INA: measurement
 *  INB: channel status
 *  INC: xstart
 *  IND: xstop
 *  INE: data points
 *  INF: method for fetching real data
 *  ING: real5 (5 digits to skip over for head string)
 *  INH: real6 (6 digits to skip over for head string)
 *
 *  VALA: xaxis
 *  VALB: yaxis
 *
 */
static int copying_waveform_real(aSubRecord *precord)
{
    const short mesWF	= *((short*)precord->a);
    const short channel	= *((short*)precord->b);
    const long points	= *((long*)precord->e);
    if (points>MAXPOINTS) {
        printf("[aSub ERROR] data points is too high\n");
        return -1;
    }
    // copying waveform
    if (mesWF==0 || (mesWF==1 && channel==0)) {
        double null[]={};
        memcpy(precord->vala, null, 0*sizeof(double));
        precord->neva=0;
        memcpy(precord->valb, null, 0*sizeof(double));
        precord->nevb=0;
    }
    else if (mesWF==1 && channel==1) {
        const double xstart = *((double*)precord->c);
        const double xstop  = *((double*)precord->d);
        const short format  = *((short*)precord->f);
        int i;
        double *input_wave;
        double *xaxis, *yaxis;
        xaxis = (double *)malloc(points*sizeof(double));
        if (xaxis == NULL) {
            printf("[asub Error] cannot allocate memory");
            return -1;
        }
        yaxis = (double *)malloc(points*sizeof(double));
	if (yaxis == NULL) {
            printf("[asub Error] cannot allocate memory");
            return -1;
        }
        if (format==0) {
	    input_wave = (double*)precord->g;
            for (i=0; i<points; i++) {	
                xaxis[i]=xstart+i*(xstop-xstart)/points;
                yaxis[i]=input_wave[i];
            }
        }
        else if (format==1) {
            input_wave = (double*)precord->h;
            for (i=0; i<points; i++) {	
                xaxis[i]=xstart+i*(xstop-xstart)/points;
                yaxis[i]=input_wave[i];
            }
        }
        else {
            printf("[aSub ERROR] invalid real data\n");
            return -1;
        }
        memcpy(precord->vala,xaxis,points*sizeof(double));
        precord->neva=points;
        memcpy(precord->valb,yaxis,points*sizeof(double));
        precord->nevb=points;
        free(xaxis),free(yaxis);
    }
    /* Done! */
    return 0;
}
epicsRegisterFunction(parsing_waveform_uint16);
epicsRegisterFunction(copying_waveform_real);
