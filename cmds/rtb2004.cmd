require stream
require rs_rtb2004
require iocstats

epicsEnvSet("TOP",	"$(E3_CMD_TOP)/..")
epicsEnvSet("IOCNAME",	"TS2-010RFC:SC-IOC-010")
epicsEnvSet("P",        "TS2-010CRM:")
epicsEnvSet("R",        "EMR-SCOPE-010:")
epicsEnvSet("CH1",      "1")
epicsEnvSet("CH2",      "2")
epicsEnvSet("CH3",      "3")
epicsEnvSet("CH4",      "4")
epicsEnvSet("ASYN_PORT","RTB2004")
epicsEnvSet("IP",	"192.168.1.3")
#epicsEnvSet("IP",       "scope-ts2.tn.esss.lu.se")

# Add extra startup scripts requirements here
iocshLoad("$(iocstats_DIR)/iocStats.iocsh")
iocshLoad("${rs_rtb2004_DIR}rs_rtb2004.iocsh", "P=$(P), R=$(R), IP=$(IP), CH1=$(CH1), CH2=$(CH2), CH3=$(CH3), CH4=$(CH4), ASYN_PORT=$(ASYN_PORT)")

# Start any sequence programs
seq("sncProcess","P=$(P), R=$(R)")

# Call iocInit to start the IOC
iocInit()
